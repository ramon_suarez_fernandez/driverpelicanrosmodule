#ifndef PELICAN_COMMAND_FILTER_H
#define PELICAN_COMMAND_FILTER_H

#include <ros/ros.h>
#include <asctec_msgs/CtrlInput.h>
#include <mav_srvs/SetMotorsOnOff.h>
#include <mav_srvs/GetMotorsOnOff.h>
#include "xmlfilereader.h"

class PelicanCommandFilter
{
private:
//    ros::ServiceServer set_motors_on_off_srv_;
//    ros::ServiceServer get_motors_on_off_srv_;
//    void startMotors();
//    void stopMotors();
//    bool setMotorsOnOff(mav_srvs::SetMotorsOnOff::Request  &req,
//                        mav_srvs::SetMotorsOnOff::Response &res);
//    bool getMotorsOnOff(mav_srvs::GetMotorsOnOff::Request  &req,
//                        mav_srvs::GetMotorsOnOff::Response &res);

    // Configuration parameters
    // if true, allow motor on/off service, IMPORTANT: Currently not developed
    bool enable_state_changes_;
    // enabling of pelican control channels
    bool enable_ctrl_thrust_;
    bool enable_ctrl_roll_;
    bool enable_ctrl_pitch_;
    bool enable_ctrl_yaw_;
    // saturation of pelican commands (min/max output - in asctec units)
    int max_ctrl_thrust_;
    int min_ctrl_thrust_;
    int max_ctrl_roll_;
    int max_ctrl_pitch_;
    int max_ctrl_yaw_;

private:
    void initializeParams(int droneId);

public:
    PelicanCommandFilter(int droneId);
    void open(ros::NodeHandle & nIn, std::string moduleName);

    void assembleCtrlCommands(asctec_msgs::CtrlInput &msg);
};

#endif // PELICAN_COMMAND_FILTER_H
