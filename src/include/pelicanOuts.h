//////////////////////////////////////////////////////
//  parrotARDroneOuts.h
//
//  Created on: Jul 3, 2013
//      Author: joselusl
//
//  Last modification on: Dec 23, 2013
//      Author: joselusl
//
//  Description: file to manage ardrone_autonomy node
//  See: https://github.com/AutonomyLab/ardrone_autonomy
//
//////////////////////////////////////////////////////


#ifndef _PELICAN_OUTS_H
#define _PELICAN_OUTS_H


#include "droneModuleROS.h"
#include "communication_definition.h"
#include "sensor_msgs/Imu.h"
//Battery
#include "droneMsgsROS/battery.h"
//IMU
#include "asctec_msgs/IMUCalcData.h"
//RotationAngles
#include "geometry_msgs/Vector3Stamped.h"
//Battery
#include "asctec_msgs/LLStatus.h"
//// ROS  ///////
#include "ros/ros.h"
//I/O stream
//std::cout
#include <iostream>
//Math
//M_PI
#include <cmath>


/////////////////////////////////////////
// Class Battery
//
//   Description: Needs to be adjusted the unit!
//
/////////////////////////////////////////
class BatteryROSModule : public DroneModule
{

    //Publisher
protected:
    ros::Publisher BatteryPubl;
    bool publishBatteryValue();


    //Subscriber
protected:
    ros::Subscriber BatterySubs;
    void batteryCallback(const asctec_msgs::LLStatus::ConstPtr& msg);


    //Battery msgs
protected:
    droneMsgsROS::battery BatteryMsgs;


    //Constructors and destructors
public:
    BatteryROSModule();
    ~BatteryROSModule();


    //Init and close
public:
    void init();
    void close();

    //Open
 public:
    void open(ros::NodeHandle & nIn, std::string moduleName);

    //Reset
protected:
    bool resetValues();

    //Start
protected:
    bool startVal();

    //Stop
protected:
    bool stopVal();

    //Run
public:
    bool run();


};


/////////////////////////////////////////
// Class RotationAngles
//
//   Description
//
/////////////////////////////////////////
class RotationAnglesROSModule : public DroneModule
{

    //Publisher
protected:
    ros::Publisher RotationAnglesPubl;
    bool publishRotationAnglesValue();


    //Subscriber
protected:
    ros::Subscriber RotationAnglesSubs;
    void rotationAnglesCallback(const asctec_msgs::IMUCalcData::ConstPtr& msg);


    //RotationAngles msgs
protected:
    geometry_msgs::Vector3Stamped RotationAnglesMsgs;


    //Constructors and destructors
public:
    RotationAnglesROSModule();
    ~RotationAnglesROSModule();


    //Init and close
public:
    void init();
    void close();

    //Open
 public:
    void open(ros::NodeHandle & nIn, std::string moduleName);

    //Reset
protected:
    bool resetValues();

    //Start
protected:
    bool startVal();

    //Stop
protected:
    bool stopVal();

    //Run
public:
    bool run();


};


#endif
