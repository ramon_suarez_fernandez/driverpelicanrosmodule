//////////////////////////////////////////////////////
//  parrotARDroneInps.h
//
//  Created on: Jul 3, 2013
//      Author: joselusl
//
//  Last modification on: Dec 23, 2013
//      Author: joselusl
//
//  Description: file to manage ardrone_autonomy node
//  See: https://github.com/AutonomyLab/ardrone_autonomy
//
//////////////////////////////////////////////////////


#ifndef _PELICAN_INPS_H
#define _PELICAN_INPS_H

#include <iostream>
#include <math.h>


//// ROS  ///////
#include "ros/ros.h"

#include "droneModuleROS.h"
#include "communication_definition.h"
#include "droneMsgsROS/droneAutopilotCommand.h"
#include "asctec_msgs/CtrlInput.h"
#include "pelican_command_filter.h"

/////////////////////////////////////////
// Class DroneCommand
//
//   Description
//
/////////////////////////////////////////
class DroneCommandROSModule : public DroneModule
{
    //Constructors and destructors
public:
    DroneCommandROSModule();
    ~DroneCommandROSModule();


    //Init and close
public:
    void init();
    void close();

    //Open
 public:
    void open(ros::NodeHandle & nIn, std::string moduleName);

    //Reset
protected:
    bool resetValues();

    //Start
protected:
    bool startVal();

    //Stop
protected:
    bool stopVal();

    //Run
public:
    bool run();

protected:
    //Subscribers
    ros::Subscriber ML_autopilot_command_subscriber;
    void MLAutopilotCommandCallback(const droneMsgsROS::droneAutopilotCommand& msg);
    // Publishers
    ros::Publisher pelican_CtrlInput_publisher;

private:
    PelicanCommandFilter pelican_command_filter;
};



#endif
