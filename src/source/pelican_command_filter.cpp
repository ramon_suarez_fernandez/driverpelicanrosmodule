#include "pelican_command_filter.h"

PelicanCommandFilter::PelicanCommandFilter(int droneId) :
    enable_state_changes_(false),
    enable_ctrl_thrust_(false),
    enable_ctrl_roll_(false),
    enable_ctrl_pitch_(false),
    enable_ctrl_yaw_(false),
    max_ctrl_thrust_(4095),
    min_ctrl_thrust_(   0),
    max_ctrl_roll_(2047),
    max_ctrl_pitch_(2047),
    max_ctrl_yaw_(1700)
{
    initializeParams(droneId);
}

void PelicanCommandFilter::open(ros::NodeHandle &nIn, std::string moduleName)
{
//    if(enable_state_changes_)
//    {
//      set_motors_on_off_srv_ = nIn.advertiseService(
//        "setMotorsOnOff", &PelicanCommandFilter::setMotorsOnOff, this);
//    }
//
//    get_motors_on_off_srv_   = nIn.advertiseService(
//                "getMotorsOnOff", &PelicanCommandFilter::getMotorsOnOff, this);
}

void PelicanCommandFilter::initializeParams(int droneId)
{
    std::cout << "entering PelicanCommandFilter::initializeParams()" << std::endl;

    std::string stackPath;
    ros::param::get("~stackPath",stackPath);
    stackPath+="/";
    try {
        XMLFileReader my_xml_reader(stackPath+"configs/drone"+ cvg_int_to_string(droneId)+"/driver_pelican.xml");
        // Configuration parameters
        // if true, allow motor on/off service
        enable_state_changes_ = (bool) my_xml_reader.readIntValue( "driver_pelican_config:enable_state_changes" );
        // enabling of pelican control channels
        enable_ctrl_thrust_ = (bool) my_xml_reader.readIntValue( "driver_pelican_config:enable_ctrl_thrust" );
        enable_ctrl_yaw_    = (bool) my_xml_reader.readIntValue( "driver_pelican_config:enable_ctrl_yaw"    );
        enable_ctrl_roll_   = (bool) my_xml_reader.readIntValue( "driver_pelican_config:enable_ctrl_roll"   );
        enable_ctrl_pitch_  = (bool) my_xml_reader.readIntValue( "driver_pelican_config:enable_ctrl_pitch"  );
        // saturation of pelican commands (min/max output - in asctec units)
        max_ctrl_thrust_ = my_xml_reader.readIntValue( "driver_pelican_config:max_ctrl_thrust" );
        min_ctrl_thrust_ = my_xml_reader.readIntValue( "driver_pelican_config:min_ctrl_thrust" );
        max_ctrl_roll_   = my_xml_reader.readIntValue( "driver_pelican_config:max_ctrl_roll"   );
        max_ctrl_pitch_  = my_xml_reader.readIntValue( "driver_pelican_config:max_ctrl_pitch"  );
        max_ctrl_yaw_    = my_xml_reader.readIntValue( "driver_pelican_config:max_ctrl_yaw"    );
    } catch ( cvg_XMLFileReader_exception &e) {
    throw cvg_XMLFileReader_exception(std::string("[cvg_XMLFileReader_exception! caller_function: ") + BOOST_CURRENT_FUNCTION + e.what() + "]\n");
    }

    std::cout << "PelicanCommandFilter configuration parameters:" << std::endl;
    std::cout << "enable_state_changes_:" << enable_state_changes_ << std::endl;
    std::cout << "enable_ctrl_thrust_:" << enable_ctrl_thrust_ << std::endl;
    std::cout << "enable_ctrl_yaw_   :" << enable_ctrl_yaw_    << std::endl;
    std::cout << "enable_ctrl_roll_  :" << enable_ctrl_roll_   << std::endl;
    std::cout << "enable_ctrl_pitch_ :" << enable_ctrl_pitch_  << std::endl;
    std::cout << "max_ctrl_thrust_:" << max_ctrl_thrust_ << std::endl;
    std::cout << "min_ctrl_thrust_:" << min_ctrl_thrust_ << std::endl;
    std::cout << "max_ctrl_roll_  :" << max_ctrl_roll_   << std::endl;
    std::cout << "max_ctrl_pitch_ :" << max_ctrl_pitch_  << std::endl;
    std::cout << "max_ctrl_yaw_   :" << max_ctrl_yaw_    << std::endl;

    std::cout << "exiting  PelicanCommandFilter::initializeParams()" << std::endl;
}

#define KEEP_IN_RANGE(a, min, max) if (a < min) a = min; else if (a > max) a = max;

void PelicanCommandFilter::assembleCtrlCommands(asctec_msgs::CtrlInput &msg)
{
    // command saturation
    KEEP_IN_RANGE( msg.thrust,  min_ctrl_thrust_,  max_ctrl_thrust_);
    KEEP_IN_RANGE( msg.yaw   , -max_ctrl_yaw_   , +max_ctrl_yaw_   );
    KEEP_IN_RANGE( msg.roll  , -max_ctrl_roll_  , +max_ctrl_roll_  );
    KEEP_IN_RANGE( msg.pitch , -max_ctrl_pitch_ , +max_ctrl_pitch_ );

    // ctrl mask (enabling of control channels)
    msg.ctrl  = int(0b0000);
    if (enable_ctrl_thrust_) msg.ctrl |= 0b1000;
    if (enable_ctrl_yaw_)    msg.ctrl |= 0b0100;
    if (enable_ctrl_roll_)   msg.ctrl |= 0b0010;
    if (enable_ctrl_pitch_)  msg.ctrl |= 0b0001;

    // update checksum and timestamp
    msg.chksum = (short)0xAAAA + msg.thrust + msg.ctrl + msg.pitch + msg.roll + msg.yaw;
    msg.header.stamp = ros::Time::now();
}
