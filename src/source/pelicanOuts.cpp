//Drone
#include "pelicanOuts.h"

using namespace std;

////// Battery ////////
BatteryROSModule::BatteryROSModule() : DroneModule(droneModule::active)
{
    init();
    return;
}

BatteryROSModule::~BatteryROSModule()
{

    return;
}

void BatteryROSModule::init()
{

}

void BatteryROSModule::close()
{

}

void BatteryROSModule::open(ros::NodeHandle & nIn, std::string moduleName)
{
    //Node
    DroneModule::open(nIn,moduleName);

    //init();


    //Configuration


    //Publisher
    BatteryPubl = n.advertise<droneMsgsROS::battery>(DRONE_DRIVER_SENSOR_BATTERY, 1, true);


    //Subscriber
    BatterySubs=n.subscribe("asctec/LL_STATUS", 1, &BatteryROSModule::batteryCallback, this);


    //Flag of module opened
    droneModuleOpened=true;

    //Auto-Start module
    moduleStarted=true;

    //End
    return;
}

//Reset
bool BatteryROSModule::resetValues()
{
    return true;
}

//Start
bool BatteryROSModule::startVal()
{
    return true;
}

//Stop
bool BatteryROSModule::stopVal()
{
    return true;
}

//Run
bool BatteryROSModule::run()
{
    if(!DroneModule::run())
    {
        return false;
    }

    return true;
}


void BatteryROSModule::batteryCallback(const asctec_msgs::LLStatus::ConstPtr& msg)
{
    //Asynchronous module with only one callback!
    if(!run())
        return;

    //Read Battery from navdata
    BatteryMsgs.header=msg->header;
    // minimum voltage per cell as explained by IFMB is 3.30 V
    // msg->battery_voltage_1 is in mV
    BatteryMsgs.batteryPercent = ( ((double)msg->battery_voltage_1)/(1000.0) -  9.9 ) * 100.0 / 2.7;

    publishBatteryValue();
    return;
}


bool BatteryROSModule::publishBatteryValue()
{
    if(droneModuleOpened==false)
        return false;

    BatteryPubl.publish(BatteryMsgs);
    return true;
}



////// RotationAngles ////////
RotationAnglesROSModule::RotationAnglesROSModule() : DroneModule(droneModule::active)
{
    init();
    return;
}

RotationAnglesROSModule::~RotationAnglesROSModule()
{

    return;
}



void RotationAnglesROSModule::init()
{

}

void RotationAnglesROSModule::close()
{

}

void RotationAnglesROSModule::open(ros::NodeHandle & nIn, std::string moduleName)
{
    //Node
    DroneModule::open(nIn,moduleName);

    //init();


    //Configuration


    //Publisher
    RotationAnglesPubl = n.advertise<geometry_msgs::Vector3Stamped>(DRONE_DRIVER_SENSOR_ROTATION_ANGLES, 1, true);


    //Subscriber
    RotationAnglesSubs=n.subscribe("asctec/IMU_CALCDATA", 1, &RotationAnglesROSModule::rotationAnglesCallback, this);


    //Flag of module opened
    droneModuleOpened=true;

    //Auto-Start module
    moduleStarted=true;

    //End
    return;
}

//Reset
bool RotationAnglesROSModule::resetValues()
{
    return true;
}

//Start
bool RotationAnglesROSModule::startVal()
{
    return true;
}

//Stop
bool RotationAnglesROSModule::stopVal()
{
    return true;
}

//Run
bool RotationAnglesROSModule::run()
{
    if(!DroneModule::run())
    {
        return false;
    }

    return true;
}


void RotationAnglesROSModule::rotationAnglesCallback(const asctec_msgs::IMUCalcData::ConstPtr& msg)
{
    //Asynchronous module with only one callback!
    if(!run())
        return;

    // Note, units of: msg->angle_nick, msg->angle_roll, msg->angle_yaw are mdeg (1000mdeg = deg).

    RotationAnglesMsgs.header=msg->header;
    // deg,   mavwork reference frame
    RotationAnglesMsgs.vector.x =  ((double)msg->angle_roll)/1000.0;  // deg
    RotationAnglesMsgs.vector.y =  ((double)msg->angle_nick)/1000.0;  // deg
    RotationAnglesMsgs.vector.z =  ((double)msg->angle_yaw )/1000.0;  // deg

    publishRotationAnglesValue();
    return;
}


bool RotationAnglesROSModule::publishRotationAnglesValue()
{
    if(droneModuleOpened==false)
        return false;

    RotationAnglesPubl.publish(RotationAnglesMsgs);
    return true;
}
