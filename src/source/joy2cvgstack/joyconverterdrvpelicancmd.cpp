#include "joy2cvgstack/joyconverterdrvpelicancmd.h"


//// Source of Pelican values:
//// link: https://bitbucket.org/mrtc/starmac-ros-pkg/src/0ee1cb5ee0ce7d99a5cd9cc2975d455aed43c912/starmac_robots/starmac_robots_asctec/nodes/marslab_adapter.cpp?at=default
//// Per email from AscTec, "25 millidegree" per count:
//// """The scaling of the roll/pitch/yaw axes is linear. Multiply the values
//// with 25 millidegree (+-2047 * 25 millidegree = +-51.175 degree)."""
//static double ROLL_SCALE = 40; // counts/deg
//static double PITCH_SCALE = 40; // counts/deg
//// Per email from AscTec,
//// """The standard parameter for K_stick_yaw is 120, resulting in a maximum rate of
//// 254.760 degrees per second. I.e. a 360° turn takes about 1.5 seconds."""
//static double YAW_SCALE = 2047 / 254.760; // counts/deg/s
//
//static double THRUST_SCALE = 4095 / 32; // counts/N - approximate

#define LL_AUTOPILOT_COMMAND_TILT_MIN_VALUE   ( -51.175)    // deg      int_min: -2047
#define LL_AUTOPILOT_COMMAND_TILT_MAX_VALUE   ( +51.175)    // deg      int_max:  2047
#define LL_AUTOPILOT_COMMAND_DYAW_MIN_VALUE   (-254.760)    // deg/s    int_min: -2047
#define LL_AUTOPILOT_COMMAND_DYAW_MAX_VALUE   (+254.760)    // deg/s    int_max:  2047
#define LL_AUTOPILOT_COMMAND_THRUST_MIN_VALUE (   0.000)    // N        int_min:     0
#define LL_AUTOPILOT_COMMAND_THRUST_MAX_VALUE ( +32.000)    // N        int_max:  4095

JoyConverterDrvPelicanCmd::JoyConverterDrvPelicanCmd() : DroneModule(droneModule::active)
{
    init();
    return;
}

JoyConverterDrvPelicanCmd::~JoyConverterDrvPelicanCmd()
{

    return;
}

void JoyConverterDrvPelicanCmd::init()
{
    return;
}

void JoyConverterDrvPelicanCmd::close()
{
    return;
}

void JoyConverterDrvPelicanCmd::open(ros::NodeHandle & nIn, std::string moduleName)
{
    //Node
    DroneModule::open(nIn,moduleName);

    //init();


    //Configuration


    //Publisher
    OutputPubl = n.advertise<droneMsgsROS::droneAutopilotCommand>(DRONE_DRIVER_COMMAND_DRONE_LL_AUTOPILOT_COMMAND, 1, true);


    //Subscriber
    InputSubs=n.subscribe("joy", 1, &JoyConverterDrvPelicanCmd::inputCallback, this);


    //Flag of module opened
    droneModuleOpened=true;

    //Auto-Start module
    moduleStarted=true;

    //End
    return;
}

//Reset
bool JoyConverterDrvPelicanCmd::resetValues()
{
    return true;
}

//Start
bool JoyConverterDrvPelicanCmd::startVal()
{
    return true;
}

//Stop
bool JoyConverterDrvPelicanCmd::stopVal()
{
    return true;
}

//Run
bool JoyConverterDrvPelicanCmd::run()
{
    if(!DroneModule::run())
    {
        return false;
    }

    return true;
}

//#define KEEP_IN_RANGE(a, min, max) if (a < min) a = min; else if (a > max) a = max;

void JoyConverterDrvPelicanCmd::inputCallback(const sensor_msgs::Joy::ConstPtr& msg)
{
    //Asynchronous module with only one callback!
    if(!run())
        return;

//  # Received input from joypad "Thrustmaster, Dual Analog 3", note that this
//  #   joypad is very low quality, don't fly the Pelican with it!!!!
//  #     msg->axes[0] // stick: left -horizontal, left:+1 right:-1 //   dyaw
//  #     msg->axes[1] // stick: left -vertical  ,   up:+1  down:-1 // thrust
//  #     msg->axes[2] // stick: right-horizontal, left:+1 right:-1 //   roll
//  #     msg->axes[3] // stick: right-vertical  ,   up:+1  down:-1 //  pitch
//  std::cout << " pitch:" << msg->axes[3] << std::endl;
//  std::cout << "  roll:" << msg->axes[2] << std::endl;
//  std::cout << "  dyaw:" << msg->axes[0] << std::endl;
//  std::cout << "thrust:" << msg->axes[1] << std::endl;

//  # Desired joypad-vs-pelican behaviour
//  #   signs in the code are assigned to obtain the desired behaviour.
//  #   The yaw sign test was not done during flight, so it might be wrong.
//  #     stick: left -horizontal, left:      dyaw-leftwards   right:        dyaw-rightwards
//  #     stick: left -vertical  ,   up:     thrust-upwards     down:     thrust-downwards
//  #     stick: right-horizontal, left: horizontal-left       right: horizontal-left
//  #     stick: right-vertical  ,   up: horizontal-frontwards  down: horizontal-backwards

    droneMsgsROS::droneAutopilotCommand OutputMsgs;

    OutputMsgs.pitch  = (float) (-1.0)*(  msg->axes[3]            * LL_AUTOPILOT_COMMAND_TILT_MAX_VALUE   );
    OutputMsgs.roll   = (float) (-1.0)*(  msg->axes[2]            * LL_AUTOPILOT_COMMAND_TILT_MAX_VALUE   );
    OutputMsgs.dyaw   = (float) (-1.0)*(  msg->axes[0]            * LL_AUTOPILOT_COMMAND_DYAW_MAX_VALUE   );
    OutputMsgs.thrust = (float) (+1.0)*( (msg->axes[1] + 1.0)/2.0 * LL_AUTOPILOT_COMMAND_THRUST_MAX_VALUE );

//    // Motors power on test:
//    if (OutputMsgs.thrust > 1000) {
//        OutputMsgs.yaw    = (float) (   0);
//        OutputMsgs.thrust = (float) (   0);
//    } else {
//        OutputMsgs.yaw    = (float) ( 1.0);
//        OutputMsgs.thrust = (float) ( 0.0);
//    }

    OutputMsgs.header.stamp = ros::Time::now();

    //Publish
    OutputPubl.publish(OutputMsgs);
    return;
}
