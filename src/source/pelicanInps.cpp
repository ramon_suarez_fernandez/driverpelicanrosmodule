//Drone
#include "pelicanInps.h"

// Per email from AscTec, "25 millidegree" per count:
// """The scaling of the roll/pitch/yaw axes is linear. Multiply the values
// with 25 millidegree (+-2047 * 25 millidegree = +-51.175 degree)."""
static double  PITCH_SCALE = 40; // counts/deg
static double   ROLL_SCALE = 40; // counts/deg

// Per email from AscTec,
// """The standard parameter for K_stick_yaw is 120, resulting in a maximum rate of
// 254.760 degrees per second. I.e. a 360° turn takes about 1.5 seconds."""
static double   DYAW_SCALE = 2047 / 254.760; // counts/deg/s

static double THRUST_SCALE = 4095 / 32; // counts/N - approximate

////// DroneCommand ////////
DroneCommandROSModule::DroneCommandROSModule() :
    DroneModule(droneModule::active),
    pelican_command_filter(idDrone)
{
    init();
    return;
}

DroneCommandROSModule::~DroneCommandROSModule()
{
    return;
}

void DroneCommandROSModule::init()
{
    return;
}

void DroneCommandROSModule::close()
{
    return;
}

void DroneCommandROSModule::open(ros::NodeHandle & nIn, std::string moduleName)
{
    //Node
    DroneModule::open(nIn,moduleName);

    //Publishers
    pelican_CtrlInput_publisher = n.advertise<asctec_msgs::CtrlInput>("asctec/CTRL_INPUT", 1, true);

    //Subscribers
    ML_autopilot_command_subscriber = n.subscribe(DRONE_DRIVER_COMMAND_DRONE_LL_AUTOPILOT_COMMAND, 1, &DroneCommandROSModule::MLAutopilotCommandCallback, this);

    pelican_command_filter.open( nIn, moduleName);

    //Flag of module opened
    droneModuleOpened=true;

    //Auto-Start module
    moduleStarted=true;

    //End
    return;
}

//Reset
bool DroneCommandROSModule::resetValues()
{
    return true;
}

//Start
bool DroneCommandROSModule::startVal()
{
    return true;
}

//Stop
bool DroneCommandROSModule::stopVal()
{
    return true;
}

//Run
bool DroneCommandROSModule::run()
{
    if(!DroneModule::run())
    {
        return false;
    }

    return true;
}

#define KEEP_IN_RANGE(a, min, max) if (a < min) a = min; else if (a > max) a = max;

void DroneCommandROSModule::MLAutopilotCommandCallback(const droneMsgsROS::droneAutopilotCommand& msg)
{
    //Asynchronous module with only one callback!
    if(!run())
        return;

    // Pelican behviour:
    // CTRL_INPUT.pitch   positive >> move backwards
    // CTRL_INPUT.roll    positive >> move rightwards
    // CTRL_INPUT.yaw     positive >> rotate clockwise (as seen from above), or N > E > S > W > N > ...
    // CTRL_INPUT.thurst: positive >> move up

    // Mavwork sign reference system, sign modifiers are (+/-1.0), all seem to be (+1.0):
    // [pitch][+] move backwards
    // [pitch][-] move forward
    // [roll][+] move rightwards
    // [roll][-] move leftwards
    // [dyaw][+][speed command] rotate clockwise (as seen from above), or N > E > S > W > N > ...
    // [dyaw][-][speed command] rotate counter-clockwise (as seen from above), or N > W > S > E > N > ...
    // [dz][+][speed command] increase altitude, move upwards
    // [dz][-][speed command] decrease altitude, move downwards
    asctec_msgs::CtrlInput pelican_autopilot_command_msg;
    pelican_autopilot_command_msg.pitch  = (int16_t) ( (+1.0) * msg.pitch  *  PITCH_SCALE );
    pelican_autopilot_command_msg.roll   = (int16_t) ( (+1.0) * msg.roll   *   ROLL_SCALE );
    pelican_autopilot_command_msg.yaw    = (int16_t) ( (+1.0) * msg.dyaw   *   DYAW_SCALE );
    pelican_autopilot_command_msg.thrust = (int16_t) ( (+1.0) * msg.thrust * THRUST_SCALE );

    KEEP_IN_RANGE( pelican_autopilot_command_msg.pitch,  -2047, +2047)
    KEEP_IN_RANGE( pelican_autopilot_command_msg.roll,   -2047, +2047)
    KEEP_IN_RANGE( pelican_autopilot_command_msg.yaw,    -2047, +2047)
    KEEP_IN_RANGE( pelican_autopilot_command_msg.thrust,     0, +4095)

    // This function call adds, taking into account configuration:
    //    - command saturation
    //    - ctrl mask (enabling of control channels)
    //    - chcksum
    pelican_command_filter.assembleCtrlCommands( pelican_autopilot_command_msg );

    pelican_CtrlInput_publisher.publish( pelican_autopilot_command_msg );
    return;
}
